# S3 Bucket Remediation

This project is to Remdiate S3 resources when created with Public Access to Private, using AWS Config Rule, EventBridge and Lambda. SNS is used as a notifcation system.

## Problem Statement
Your Senior Management is concerned about engineers creating Public facing S3 buckets that are meant to be private. They want you to track situations like this and if you find any, they want to remediate them using Lambda. After the remediation is complete, they want you to send a notification to the security department about which buckets were remediated. You determine what your Lambda function should do.

### Your solution should:
*  A. Detect any Public S3 Buckets (Hint: Use AWS Config)
*  B. Remediate the bucket(s) and make them private.
*  C. Send an SNS notification of which bucket(s) were remediated.

##### Note: AWS Config is not a free service. Be sure to delete your solutions after completion.
