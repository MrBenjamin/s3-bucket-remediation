# SOLUTION
To get started, please sign up or have access to an [Amazon Web Service](https://aws.amazon.com) account

# Architecture
![Architecture Diagram](https://gitlab.com/MrBenjamin/s3-bucket-remediation/-/raw/main/picture/Architecture.jpg?inline=false)

# REMEDIATION
The remediation of the public access to private was done using the following AWS services:
- AWS Config
- Lambda 
- AWS Event Bridge 

AWS Config was used to identify the non-compliant S3 buckets. A lambda function was used to check for the non-complaint S3 buckets and remediate them to private, also outputing the result of the action taken. I set up two AWS Events to trigger my Lambda function. The first is based on Config Compliance changes and second is a scheduled event that runs every 20 minutes to trigger the Lambda function.

### Step 1 - Login
*  Log in to your [Amazon Web Service](https://aws.amazon.com) account

### Step 2 - Create S3 Resources
*  Search for S3 in the services search bar 
*  In S3, create 4 buckets (2 Public buckets and 2 Private buckets)

### Step 3 - Create IAM Role 
In order for the different services in AWS to reach out to each and access the data those services have, we need to create a role with the necessary permissions and assign the role to the service that requires the permission. For this project one of the services we would be using is Lambda. Lambda would need to be able to read resources in S3, SNS and Config. 
*  Search IAM in the search bar 
*  Once in IAM dashboard you would see a tab titled role, click to open the list of roles
*  To create new role, click create role
*  Select the service that require the role, in this case Lambda and the click on the permissions button to add permissions
*  AWS already has a list of managed permissions you can select from, otherwise you can create your own policy by clicking policy in the IAM dashboard and selecting the access you want the policy to provide
*  Attatch your policy (Policy should have S3 full access, SNS full access and Config full access) to the service and create the role

 > At the time of this documentation, no special role is required for Config

### Step 4 - Create Config Rule 
*  Search Config in the search bar 
*  If this is the first time using Config select Get started 
*  Configure general settings (you can leave as default) and click next
*  Select the rule that Config would use to detect Complaint and Non-compliant resources `s3-bucket-level-public-access-prohibited `
*  Once config is running you should have `2 Compliant Resources` and `2 Non-compliant Resources`
<details><summary>Click to see config rule setup</summary>
![Config Console](https://gitlab.com/MrBenjamin/s3-bucket-remediation/-/raw/main/picture/Screen_Shot_2021-11-17_at_9.40.21_PM.png?inline=false)
</details>

### Step 5 - Create EventBridge 
*  Search Eventbridge in the search bar 
*  In the Eventbridge dashboard create rule 
*  For the first rule, enter rule name and select Event pattern: `The first rule is to trigger lambada if there are any changes to Config Compliance status `
<details><summary>Click to see event pattern set up </summary>
![Event Pattern Console](https://gitlab.com/MrBenjamin/s3-bucket-remediation/-/raw/main/picture/Screen_Shot_2021-11-17_at_10.14.50_PM.png?inline=false)
</details>

*  Select the target to be your intended Lambda function 
*  For the second rule, enter rule name and select scheduled events: `The second rule is to trigger lambada after a specified period of time `
<details><summary>Click to see scheduled event set up </summary>
![Scheduled event](https://gitlab.com/MrBenjamin/s3-bucket-remediation/-/raw/main/picture/Screen_Shot_2021-11-17_at_10.31.36_PM.png?inline=false)
</details>

### Step 6 - Simple Notification Service
*  Search SNS in the search bar 
*  In the SNS dashboard create a new topic
*  Select type, for this project I used standard, input name of Topic and click create
*  Once topic is created, select the topic and click on create subscription 
*  Select protocol, for this project we used email. Type in your email address and click create subscription 

`In order to complete subscription, click on the link in the email sent to the email provided. `
 

### Step 7 - Create Lambda Function
*  Search Lambda in the search bar 
*  In the Lambda dashboard create a new function, author from scratch 
*  Change deafult execution role and attatch IAM role created  in step 3
* In order to write your python code you have to import boto3, click here to see [boto 3 config client](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/config.html)

<details><summary>Click to See Lambda Python Code</summary>

    `import json
    import boto3
    s3_client = boto3.client('s3')
    config_clent = boto3.client('config')

        def lambda_handler(event, context):
            #get all non-compliant resources from config
            response = config_clent.get_compliance_details_by_config_rule(
            ConfigRuleName='s3-bucket-level-public-access-prohibited',
            ComplianceTypes = ['NON_COMPLIANT'],
            )
            print(response)
            #non_compliant=json.loads(json.dumps(response, default=str))
    
            eval_result=response.get('EvaluationResults')
            print(eval_result)
    
            non_comp_rsc= []
            if eval_result:
                for e in eval_result:
                    non_comp_rsc.append(e.get('EvaluationResultIdentifier').get('EvaluationResultQualifier').get('ResourceId'))
                print(non_comp_rsc)
                #the next code will encrypt the bucket
    
            for non_comp in non_comp_rsc:
                response = s3_client.put_public_access_block(
                    Bucket= non_comp,
                    PublicAccessBlockConfiguration={
                        "BlockPublicAcls": True,
                        "IgnorePublicAcls": True,
                        "BlockPublicPolicy": True,
                        "RestrictPublicBuckets": True
                }
                )
                print(response)
    
            if eval_result:
                #sns notification
                return "I found " + str(len(eval_result)) + " resource(s) that are non_compliant and have done the remediation you sent me and sent an SNS  notification summary"
            else:   
                return "All buckets are compliant"`
</details>

<details><summary>Click to see Lambda Workflow</summary>
![Lambda Workf Flow](https://gitlab.com/MrBenjamin/s3-bucket-remediation/-/raw/main/picture/Screen_Shot_2021-11-14_at_3.42.00_PM.png?inline=false)
</details>
   


